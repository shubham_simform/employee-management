Rails.application.routes.draw do
    
  
  devise_for :users, controllers: { registrations: "users/registrations" }

  get 'employees/index'
  root to: 'employees#index'
  
  resources :departments

end
