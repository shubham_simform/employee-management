class EmployeesReflex < ApplicationReflex


  def search
    @search_str = element[:value].strip
    load_records
  end

  def filter
    @filter_by = element[:value].strip
    load_records
  end

  private

    def load_records
      @employees = User.all
      @employees = @employees.search(@search_str) if @search_str
      @employees = @employees.filter_by(@filter_by) if @filter_by
    end
end
