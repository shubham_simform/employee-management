json.extract! department, :id, :name, :archive, :created_at, :updated_at
json.url department_url(department, format: :json)
