class User < ApplicationRecord

  devise :database_authenticatable, :registerable, :confirmable, :lockable,
         :recoverable, :rememberable, :validatable

  validates :first_name, :last_name, presence: true
  validates :contact, numericality: { only_integer: true }, allow_blank: true
  validates :contact, length: { is: 10 }, allow_blank: true

  belongs_to :department

  before_validation :assign_department, on: :create

  scope :search, ->(query_str) { where("name LIKE ?", "%#{query_str}%") }
  scope :filter_by, ->(department) { where('department_id = ?', department) }


  def full_name
    "#{first_name} #{last_name}".strip || email
  end

  private

  def assign_department
    self.department_id = 1#Department.first
  end
  
end
